#Установка необходимых пакетов для работы cuda с docker, так как весь инференс имодели в докере

curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://nvidia.github.io/libnvidia-container/stable/ubuntu22.04/amd64 /" | \
sudo tee /etc/apt/sources.list.d/nvidia.list > /dev/null 
sudo apt update
sudo apt install nvidia-docker2 nvidia-container-runtime -y
sudo systemctl restart docker

#docker run -d -p 18123:8123 -p 19000:9000 --name some-clickhouse-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server

#Запуск miniconda для удобной среды работы с .ipynb-файлами
docker run --gpus all -i -t -p 8888:8888 \
-v /root/www.cbr.ru:/opt/notebooks/www.cbr.ru \
-v /root/cbr_pdf_structured:/opt/notebooks/cbr_pdf_structured \
-v /root/cbr_docs_structured:/opt/notebooks/cbr_docs_structured \
-v /root/notebooks:/opt/notebooks/notebooks \
--name some-juputer-server \
continuumio/miniconda3 /bin/bash -c "/opt/conda/bin/conda install jupyter -y --quiet && mkdir -p /opt/notebooks && /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root"

#НЕ ИСПОЛЬЗУЕМ ЗАПУСКАЛИ ТОЛЬКО ДЛЯ ТЕСТОВ
docker cp CMD_FLAGS.txt text-generation-webui-text-generation-webui-1:/home/app/text-generation-webui/CMD_FLAGS.txt

#ЗАПУСТИЛИ 3 ШТУКИ
docker run -it --gpus all -p 8000:8000 -e MODEL=intfloat/multilingual-e5-large -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v1-01
docker run -it --gpus all -p 8001:8000 -e MODEL=intfloat/multilingual-e5-large -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v1-02
docker run -it --gpus all -p 8002:8000 -e MODEL=intfloat/multilingual-e5-large -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v1-03
#docker run -it --gpus all -p 8003:8000 -e MODEL=intfloat/multilingual-e5-large -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v1-04
#docker run -it --gpus all -p 8004:8000 -e MODEL=intfloat/multilingual-e5-large -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v1-05
#docker run -it --gpus all -p 8005:8000 -e MODEL=intfloat/multilingual-e5-large -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v1-06

#НЕ ИСПОЛЬЗУЕМ В ИТОГЕ
#docker run -it --gpus all -p 8002:8000 -e MODEL=cointegrated/rubert-tiny2 -e DEVICE=cuda vokturz/fast-embeddings-api --name some-embedding-server-v2-01

#LORAX
docker run --gpus all --shm-size 1g -p 8080:80 --name some-lorax-server -v ./text-generation-webui/models/:/data \
    ghcr.io/predibase/lorax:latest --model-id /data/Open-Orca_Mistral-7B-OpenOrca --adapter-id IlyaGusev/saiga_mistral_7b_lora # --max-total-tokens 8192

#ВЫГРУЗКА ДОКУМЕНТОВ
wget -r -A.pdf -np -N -e robots=off https://www.cbr.ru # PDF #ПОЛОЖИЛ В ДИРЕКТОРИЮ /root/cbr_pdf_structured
wget -m -k -K -E -l 7 -t 6 -w 5 https://www.cbr.ru # HTML #ПОЛОЖИЛ В ДИРЕКТОРИЮ /root/www.cbr.ru
wget -r -A.doc,.docx,.xls,.xlsx,.odt,.txt,.csv -np -N -e robots=off https://www.cbr.ru # ОСТАЛЬНОЕ #ПОЛОЖИЛ В ДИРЕКТОРИЮ /root/cbr_docs_structured

#ЗАПУСК БАЗЫ ДАННЫХ
version: '3'
services:
  clickhouse:
    image: clickhouse/clickhouse-server:latest
    restart: unless-stopped
    volumes:
      - ./clickhouse-data:/var/lib/clickhouse/
    ports:
      - "38123:8123"
      - "39000:9000"
    environment:
      - CLICKHOUSE_USER=default
      - CLICKHOUSE_PASSWORD=default
      - CLICKHOUSE_DB=CBR
      - CLICKHOUSE_DEFAULT_ACCESS_MANAGEMENT=1
    ulimits:
      nofile:
        soft: 262144
        hard: 262144

  tabix:
    image: spoonest/clickhouse-tabix-web-client
    ports:
      - "8082:80"
    depends_on:
      - clickhouse
    restart: unless-stopped
    environment:
      - CH_NAME=clickhouse
      - CH_HOST=clickhouse  # Use service name instead of localhost
      - CH_LOGIN=default
      - CH_PASSWORD=default
